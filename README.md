# GitLab A11y Machine Exporter

# Contributing to the  GitLab A11y Machine Exporter

We welcome contributions and improvements, please see the [contribution guidelines](CONTRIBUTING.md).

# License

This code is distributed under the MIT license, see the [LICENSE](LICENSE) file.

# See Also

The GitLab A11y Machine Exporter was inspired by the [GitLab SiteSpeed Exporter](https://gitlab.com/gitlab-org/gl-performance).