#!/bin/sh

git clone https://github.com/liip/TheA11yMachine.git a11ym
cd a11ym

# Until a new release is created with the fix for #126, need to pin to this working commit
git checkout fb6acb811018ba94af9e76e81c6498e606bd25a1

# Build and test
npm install
./a11ym -h