FROM node:8.15

COPY a11ym/clone-and-build.sh /

RUN /clone-and-build.sh

WORKDIR /a11ym

ENTRYPOINT [ "./a11ym" ] 